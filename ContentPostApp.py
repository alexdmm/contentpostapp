"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp
FORM = """
    <hr>
    <form action="/" method="post">
      <div>
        <label>Resource: </label>
        <input type="text" name="resource" required>
      </div>
      <div>
        <label>Content: </label>
        <textarea name="content" rows="5" cols="33" required></textarea>
      </div>
      <div>
        <input type="submit" value="Submit">
      </div>
    </form>
"""

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Content for {resource}:</p> {content}
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <div>
      <p>Resource not found: {resource}.</p>
    </div>
    <div>
      {form}
    </div>
  </body>
</html>
"""

PAGE_NOT_ALLOWED = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Method not allowed: {method}.</p>
  </body>
</html>
"""

PAGE_UNPROCESABLE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Unprocesable POST: {body}.</p>
  </body>
</html>
"""


class contentApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page',
               '/pepito': 'pagina de pepito'
               }

    # metodo HTTP recurso HTTP cuerpoHTTP
    # GET /pepito -----> lectura
    # POST /pepito "pagina de pepito" -----> escritura

    def parse(self, request):
        """Return the method name and resource name"""

        data = {}
        body_start = request.find('\r\n\r\n')
        if body_start == -1:
            data['body'] = None
        else:
            data['body'] = request[body_start + 4:]
        parts = request.split(' ', 2)
        data['method'] = parts[0]
        data['resource'] = parts[1]
        return (data)

    def process(self, data):
        """Produce the page with the content for the resource"""

        if data['method'] == 'GET':
            if resource in self.contents:
                content = self.contents[resource]
                page = PAGE.format(content=content, resource=resource, form=FORM)
                code = "200 OK"
            else:
                page = PAGE_NOT_FOUND.format(resource=resource, form=FORM)
                code = "404 Resource Not Found"
            return code, page
        elif data['method'] == 'POST':
            fields = parse.parse_qs(body)
            print("Fields:", fields)
            if (resource == '/'):
                if ('resource' in fields) and ('content' in fields):
                    resource = fields['resource'][0]
                    content = fields['content'][0]
                    self.contents[resource] = content
                    page = PAGE.format(content=content, resource=resource, form=FORM)
                    code = "200 OK"
                else:
                    page = PAGE_UNPROCESABLE.format(body=body)
                    code = "422 Unprocessable Entity"
            else:
                code = "405 Method not allowed"
                page = PAGE_NOT_ALLOWED.format(method='POST')
            return code, page
        else:
            code, page = "405 Method not allowed", \
                         PAGE_NOT_ALLOWED.format(method=data['method'])
        return (code, page)


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)